from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.contrib.auth import logout
from django.shortcuts import redirect

from .models import Contenido

# Create your views here.
formulario = """
No existe valor en la base de datos para esta llave.
<p>Introdúcela:
<p>
<form action= "" method="POST">
    valor: <input type="text" name="valor">
    <br/><input type="submit" value="Enviar">
</form>
"""

@csrf_exempt
def get_content(request, llave):
    if request.method == "PUT":
        valor = request.body.decode('utf-8')
        if request.user.is_authenticated: #Si se encuentra autenticado puede camabiar contenido
            try:
                c = Contenido.objects.get(clave=llave)
                c.valor = valor
            except Contenido.DoesNotExist:
                c = Contenido(clave=llave, valor=valor)
            c.save()  #Guardamos Base de datos
        else:
            respuesta = "Se debe de autenticar para modificar"
        return HttpResponse(respuesta)

    elif request.method == "POST":
        valor = request.POST['valor']
        c = Contenido(clave=llave, valor=valor)
        c.save()  #Guardamos en base de datos

    try:
        contenido = Contenido.objects.get(clave=llave)
        respuesta = contenido.valor
    except Contenido.DoesNotExist:
        if request.user.is_authenticated:    #Si se encuentra autenticado puede añadir contenido
            respuesta = "Eres " + request.user.username + "<br>"
            respuesta += formulario
        else:
            respuesta = "No puedes introducir nada si no estas autenticado. <a href='/login'>Registrate aquí!</a>"
    return HttpResponse(respuesta)

def index(request):
    content_list = Contenido.objects.all()
    template = loader.get_template('cms/index.html')
    context = {
        'content_list': content_list
    }
    return HttpResponse(template.render(context, request))


def loggedIn(request):  #Iniciar sesion
    if request.user.is_authenticated:
        respuesta = "Registrado como: " + request.user.username
    else:
        respuesta = "No se encuentra registrado. <a href='/login'>Registrate Aquí!</a>"
    return HttpResponse(respuesta)


def logout_view(request):  #Cerrar sesion
    logout(request)
    return redirect('/cms/')

